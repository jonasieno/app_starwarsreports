// Package main implements the application that provide Star Wars` reports, through the swReports and swapi´s packages. 
package main

import (
    "fmt"
)

func CliInit() {

}

func CliShowStatus(status string) {
    fmt.Println("Status: ", status)
    fmt.Println("")
}

// Func CliHandle starts the CLI user interface.
func CliShowHeader() {
    fmt.Println("---             G E T R A K            ---")
    fmt.Println("---   Star Wars Reports Application    ---")
    fmt.Println("--- Wrote by jonasieno@gmail.com V1R00 ---")
    fmt.Println("")
}

func CliGetDistance() int {
    var distance int

    fmt.Print("Insert the distance: ")
    if _, err := fmt.Scan(&distance); err != nil {
        distance = 0
    }

    fmt.Println("")

    return distance
}

func CliGetReportType() string {

    return "StarshipsStopsNeededByDistanceReport"
}

func CliShowReportResult(result string) {
    fmt.Println("The result is:")
    fmt.Println("NAME : STOPS NEEDEDS")
    fmt.Println(result)
}
