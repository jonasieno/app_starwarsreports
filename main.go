// Package main implements the application that provide Star Wars` reports, through the swReports and swapi´s packages. 
package main

import (
    "flag"
    "strconv"
    "bitbucket.org/jonasieno/swreports"
)

// Func main is the main application implementation.
// It can use cli view or web view to interact with users.
func main() {

    var distance     int
    var serverPort int  // Todo
    var debugEnabled bool

    flag.IntVar (&serverPort  , "serverport", 0      , "-server=<port> habilita a interface WEB na porta TCP <port>.")
    flag.BoolVar(&debugEnabled, "debug"     , false  , "-debug=true habilita mensagens de debug.")

    flag.Parse()

    ShowHeader()

    distance = GetDistance()

    ShowStatus("Processing...")
    starshipsStopsNeeded := swreports.StarshipsStopsNeededByDistanceReport(distance, debugEnabled)

    var reportResult string

    for _, starshipStopsNeeded := range starshipsStopsNeeded {
        reportResult = reportResult + starshipStopsNeeded.Name + " : " + strconv.Itoa(starshipStopsNeeded.StopsNeeded) + "\r\n"
    }

    ShowReportResult(reportResult)

}