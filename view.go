// Package main implements the application that provide Star Wars` reports, through the swReports and swapi´s packages. 
package main

var ServerPort int
var isWeb      bool

func ViewInit(serverPort int) {
    ServerPort = serverPort
    
    if ServerPort > 0 {
        isWeb = true
        WebInit()
    } else {
        CliInit()
    }
}


func ShowStatus(status string) {

    if isWeb {
        // Todo
    } else {
        CliShowStatus(status)
    }
}

// Func CliHandle starts the CLI user interface.
func ShowHeader() {

    if isWeb {
        // Todo
    } else {
        CliShowHeader()
    }
}


func GetDistance() int {
    var distance int

    if isWeb {
        // Todo
    } else {
        distance = CliGetDistance()
    }

    return distance
}

func GetReportType() string {

    return "StarshipsStopsNeededByDistanceReport"
}

func ShowReportResult(result string) {
    if isWeb {
        // Todo
    } else {
        CliShowReportResult(result)
    }

}
